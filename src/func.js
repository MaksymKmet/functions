const getSum = (str1, str2) => {
  if ( isNaN(str1) || isNaN(str2) ) {
    return false;
  }
  if ( typeof str1 === 'object' || typeof str2 === 'object' || typeof str1 === 'array' || typeof str2 === 'array' ) {
    return false;
  }
  return String(Number(str1) + Number(str2));
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let numberOfPosts = 0;
  let numberOfComments = 0;

  listOfPosts.forEach( elem => {
      if (elem.author === authorName) {
          ++numberOfPosts;
      }
      if (elem.comments) {
          elem.comments.forEach( comm => {
              if (comm.author === authorName) {
                  ++numberOfComments;
              }
          });    
      }
  });
  return `Post:${numberOfPosts},comments:${numberOfComments}`;
};

const tickets=(people)=> {
  let cashRegister = 0;
    for ( let i = 0; i < people.length; i++) {
        const bill = +people[i];
        switch (bill) {
            case 25:
                cashRegister += bill;
                break;
            case 50:
                if (cashRegister >= 25) {
                    cashRegister += 25;
                } else {
                    return 'NO';
                }
                break;
            case 100:
                if (cashRegister >= 75) {
                    cashRegister += 25;
                } else {
                    return 'NO';
                }
            break;
        }
    };
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};